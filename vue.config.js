const isDev = process.env.NODE_ENV !== 'production'

// 设置项目版本
process.env.VUE_APP_VERSION = require('./package.json').version

module.exports = {
  outputDir: 'dist',
  assetsDir: '',
  indexPath: 'index.html',
  filenameHashing: true,
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: 'vue 后台管理系统'
    }
  },
  lintOnSave: isDev,
  runtimeCompiler: false,
  transpileDependencies: [],
  productionSourceMap: true,
  crossorigin: undefined,
  integrity: false,
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/assets/css/global-variables.scss";`
      }
    }
  },
  devServer: {
    overlay: {
      warnings: true,
      errors: true
    },
    proxy: {
      '/api/': {
        target: 'http://localhost:3000/',
        ws: false
      }
    }
  },
  pluginOptions: {}
}
