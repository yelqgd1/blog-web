import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { get, post } from './util/axios'
const dumbFn = () => {}

import ElementUI from 'element-ui'
import '@/assets/css/element-variables.scss'
import '@/assets/css/reset.css'

Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.$request = async function({
  method = 'GET',
  url,
  data,
  config,
  onBefore = dumbFn,
  onSuccess = dumbFn,
  onFail = dumbFn,
  onFinally = dumbFn
}) {
  try {
    onBefore()

    const res = await (method === 'GET' ? get : post)(url, data, config)

    if (res && res.code === 200) {
      onSuccess(res)
    } else {
      onFail(res)
    }
  } catch (e) {
    return Promise.reject(e)
  } finally {
    onFinally()
  }
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
