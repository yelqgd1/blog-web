import axios from 'axios'
import queryString from 'query-string'
import { getUserInfoFromLocalStorage } from '@/util/local-storage'

import baseConfig from './axios.base.config'
import { getHttpErrorMessage } from './handleException'

const axiosInstance = axios.create(baseConfig)
const o = getUserInfoFromLocalStorage()
const token = o ? o.token : 'empty token'
const updateHeaderToken = token =>
  (axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`)
updateHeaderToken(token)

const printError = (colorfulMessage, ...args) =>
  // eslint-disable-next-line no-console
  console.log(
    `%c${colorfulMessage}`,
    'background:#000;color:pink;fontStyle;',
    ...args
  )

axiosInstance.interceptors.response.use(
  response => {
    if (response.config.responseType !== 'json') {
      return response
    }

    const { code, msg } = response.data
    const responseDataValid = code === 200

    if (responseDataValid) {
      return response.data
    } else {
      printError(
        `${response.config.url} CUSTOMER_ERROR_CODE: ${code}, ERROR_MESSAGE: ${msg}`
      )

      return Promise.reject(response.data)
    }
  },
  error => {
    if (
      error.code === 'ECONNABORTED' &&
      error.message.indexOf('timeout') !== -1
    ) {
      printError('网络请求超时')
    } else {
      const code = error.response.status

      printError(getHttpErrorMessage({ code }))
    }

    return Promise.reject(error)
  }
)

// config 会覆盖 basicConfig
const get = (
  url,
  params,
  config = { paramsSerializer: queryString.stringify }
) =>
  axiosInstance.get(url, {
    ...config,
    params: { ...config.params, ...params }
  })

const post = (url, data, config) => axiosInstance.post(url, data, config)

export { get, post, updateHeaderToken }
