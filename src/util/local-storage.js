const localUserInfoKey = 'YELQGD_BLOG_INFO'

export const getUserInfoFromLocalStorage = () =>
  JSON.parse(localStorage.getItem(localUserInfoKey))

export const setUserInfoToLocalStorage = data => {
  localStorage.setItem(localUserInfoKey, JSON.stringify(data))
}

export const clearUserInfoInLocalStorage = () => {
  localStorage.removeItem(localUserInfoKey)
}
