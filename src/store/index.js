import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    title: '',
    description: ''
  },

  mutations: {
    updateTitle(state, value) {
      state.title = value
    },

    updateDescription(state, description) {
      state.description = description
    }
  },
  actions: {},
  modules: {}
})
