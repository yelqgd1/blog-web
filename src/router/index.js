import Vue from 'vue'
import Router from 'vue-router'

import header from '@/components/basic-layout/header'
import aside from '@/components/basic-layout/aside'
import content from '@/components/basic-layout/content'
import main from '@/views/main'

import article from '@/views/article'
import articleDetail from '@/views/article/detail'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: main,
      children: [
        {
          path: '',
          name: 'main',
          components: {
            header,
            content,
            aside
          },
          // 此处上面三个 components 只应该有一个包含 router-view，否则下面的 children 会渲染所至所有 router-view 处
          children: [
            {
              path: '',
              name: 'articleList',
              component: article,
              meta: {
                title: '主页'
              },
              children: [
                {
                  path: ':id',
                  name: 'articleDetail',
                  component: articleDetail,
                  meta: {
                    menuKey: '/article'
                  }
                }
              ]
            }
            // {
            //   path: 'archive',
            //   name: 'archive',
            //   component: article,
            //   meta: {
            //     title: '归档'
            //   }
            // },
            // {
            //   path: 'classification',
            //   name: 'classification',
            //   component: article,
            //   meta: {
            //     title: '分类'
            //   }
            // },
            // {
            //   path: 'tag',
            //   name: 'tag',
            //   component: article,
            //   meta: {
            //     title: '标签'
            //   }
            // }
          ]
        }
      ]
    }
  ]
})

export default router
